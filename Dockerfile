FROM docker.io/library/python:3.11
WORKDIR /app
COPY ./realworld-main /app
RUN pip3 install -r /app/requirements.txt

ENV DB_PATH=/data/db.sqlite3
CMD ["/bin/sh", "-cx", "/app/manage.py migrate && exec /app/manage.py runserver 0.0.0.0:8080"]

